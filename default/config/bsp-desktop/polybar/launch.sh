#!/usr/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar(s) setting tray on 1st/primary monitor
# mapfile -t BSP_Monitors < <( sh "$BSP_DESKTOP_HOME/scripts/bsp-multihead.sh" )
mapfile -t BSP_Monitors < <(polybar -M | cut -d: -f1)
for mon in ${BSP_Monitors[@]} ; do
    MONITOR=$mon TRAYMON=$TRAYMON polybar --reload  top -c $BSP_DESKTOP_HOME/polybar/config.ini &
    TRAYMON=${TRAYMON:-none}
done
