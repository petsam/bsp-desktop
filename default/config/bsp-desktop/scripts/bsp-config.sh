#! /bin/sh

# Source settings (user settings changed with GUI)
source $BSP_DESKTOP_HOME/settings/bspwm-layout.conf

bspc config border_width			$border_width
bspc config window_gap				$window_gap
bspc config split_ratio				$split_ratio

bspc config borderless_monocle		$borderless_monocle
bspc config gapless_monocle			$gapless_monocle
bspc config single_monocle			$single_monocle
bspc config center_pseudo_tiled		$center_pseudo_tiled

bspc config pointer_follows_focus	$pointer_follows_focus
bspc config focus_follows_pointer	$focus_follows_pointer
bspc config pointer_follows_monitor $pointer_follows_monitor


# BSPWM Border colors
# Source settings (user settings changed with GUI)
source $BSP_DESKTOP_HOME/settings/default_colors.conf

bspc config focused_border_color	"$focused_color"
bspc config normal_border_color		"$normal_color"
bspc config active_border_color		"$active_color"
