#! /usr/bin/bash

XRANDR_Monitors=( $(sh $BSP_DESKTOP_HOME/scripts/bsp-multihead.sh) )
BSPC_Monitors=( A B C D E F G H I J K L M N O P Q R S T U V W X Y Z )
MonsCount=${#XRANDR_Monitors[@]}
mi=0
for mon in "${XRANDR_Monitors[@]}" ; do
	# Rename with Letter names
	bspc monitor $mon -n ${BSPC_Monitors[$mi]}
	if [[ $MonsCount -eq 1 ]]; then
		bspc monitor ${BSPC_Monitors[$mi]} -d 1 2 3 4 5 6 7 8 9 10
	elif [[ $MonsCount -eq 2 ]]; then
		bspc monitor ${BSPC_Monitors[$mi]} -d 1 2 3 4 5
	elif [[ $MonsCount -eq 3 ]]; then
		bspc monitor ${BSPC_Monitors[$mi]} -d 1 2 3 4
	elif [[ $MonsCount -ge 4 ]]; then
		bspc monitor ${BSPC_Monitors[$mi]} -d 1 2 3
	fi
	mi=$(($mi+1))
done
#bspc monitor VGA1 -d 1 2 3 4 5
#bspc monitor HDMI1 -d 6 7 8 9 10
