#!/usr/bin/bash

if [ -f /usr/bin/lxqt-policykit-agent ]; then
	/usr/bin/lxqt-policykit-agent &
elif [ -f /usr/lib/polkit-kde-authentication-agent-1 ]; then
	/usr/lib/polkit-kde-authentication-agent-1 &
elif [ -f /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 ]; then
	/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
else
	if [ -z "$DISPLAY" ] ; then
		echo "WARNING: There is no Policy Kit agent configured!"
	else
		notify-send -a "BSP Desktop" -u critical -w "Polkit Missing" "There is no Policy Kit agent configured!" &
	fi
fi
