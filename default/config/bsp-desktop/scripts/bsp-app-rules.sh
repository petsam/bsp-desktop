#! /bin/sh

# Source settings (user settings changed with GUI)
# source $BSP_DESKTOP_HOME/settings/app-rules.conf
bspc config external_rules_command  $BSP_DESKTOP_HOME/scripts/external_rules

# Open at specific desktop (Nr is the total number spanning all monitors)
bspc rule -a Chromium desktop='^4'
bspc rule -a firefox desktop='^4'
bspc rule -a Joplin desktop='^1'
bspc rule -a obs desktop='^5'
bspc rule -a TelegramDesktop desktop='^1'
bspc rule -a ViberPC desktop='^1'
bspc rule -a Vivaldi-stable desktop='^4'
