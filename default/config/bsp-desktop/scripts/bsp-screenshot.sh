#!/usr/bin/bash

# Import translations for LANGuage
TrLang=${LANG%%_*}
[ -r $BSP_DESKTOP_HOME/rofi/language/${TrLang:-en}.sh ] &&
	source $BSP_DESKTOP_HOME/rofi/language/${TrLang:-en}.sh 2>/dev/null

# Print menu options for screenshot
PrtScrn0="${PrtScrn0:-Screen}"
PrtScrn1="${PrtScrn1:-Area}"
PrtScrn2="${PrtScrn2:-Window}"
PrtScrn3="${PrtScrn0:-Screen} ${WithDelay:-with delay} 5 ${Seconds:-sec}"
PrtScrn4="${PrtScrn1:-Area} ${WithDelay:-with delay} 5 ${Seconds:-sec}"
PrtScrn5="${PrtScrn2:-Window} ${WithDelay:-with delay} 5 ${Seconds:-sec}"

# options to be displyed
options="$PrtScrn0\n$PrtScrn1\n$PrtScrn2\n$PrtScrn3\n$PrtScrn4\n$PrtScrn5"

selected="$(echo -e "$options" | rofi -dmenu -hover-select -me-select-entry '' -me-accept-entry MousePrimary \
    -theme-str 'configuration {dmenu {display-name: @ScreenShot; } }' \
	-theme-str '#window { width:22em; }' \
	-theme-str '#listview { lines:6; }')"
[ -n $XDG_PICTURES_DIR ] || {
    [ -r "$XDG_CONFIG_HOME/user-dirs.dirs" ] && source $XDG_CONFIG_HOME/user-dirs.dirs
    }
echo "$selected" | grep -q "$WithDelay" 2>/dev/null && delay=' -d 5'
case "$selected" in
    ${PrtScrn0}*)
        cd $XDG_PICTURES_DIR && sleep 1 && scrot ${delay} ;;
    ${PrtScrn1}*)
        cd $XDG_PICTURES_DIR && scrot -s ${delay} ;;
    ${PrtScrn2}*)
        cd $XDG_PICTURES_DIR && sleep 1 && scrot -u ${delay} ;;
esac

