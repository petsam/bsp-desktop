#!/usr/bin/env bash

grep -B1 --no-group-separator  "^[a-zA-Z].*" $BSP_DESKTOP_HOME/sxhkd/sxhkdrc |
	sed 'N;s/\n/%/;s/^#*[ ]*//' | column  -t -s '%' |
	rofi -dmenu -i \
	-no-show-icons \
	-theme-str 'configuration {dmenu {display-name: @KeyBindings;}}' \
	-theme-str '#listview { columns: 1; lines: 18;}'
# 	-markup-rows \
