#!/usr/bin/bash

BSP_DESKTOP_CACHE="$HOME/.cache/bsp-desktop"
# Confirm cache folder exists
mkdir -p "$BSP_DESKTOP_CACHE"

# Save input in file
cat /dev/stdin > "$BSP_DESKTOP_CACHE"/dunstdmenu

# Clean menu lines and save user choice in file
rofi -config $BSP_DESKTOP_HOME/rofi/config.rasi -dmenu -p Options \
< <( sed -n 's|^#\(.*\) (.*) \[.*\]$|\1|p' "$BSP_DESKTOP_CACHE"/dunstdmenu ) > "$BSP_DESKTOP_CACHE"/dunstdmenuchoice

# Get clean user choice
choice="$(grep . "$BSP_DESKTOP_CACHE"/dunstdmenuchoice)"

# Print dirty user choice to output for dunst
grep "$choice" "$BSP_DESKTOP_CACHE"/dunstdmenu
