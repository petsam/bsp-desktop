#! /usr/bin/bash

if [ -s "$HOME"/.screenlayout/monitor.sh ] ; then
	sh "$HOME"/.screenlayout/monitor.sh &
	exit
else
	if [ -z "$DISPLAY" ] ; then
		echo "WARNING: There was no monitor layout configuration found!"
	else
		UserChoice=$(notify-send -A arandr="Start Arandr to set monitor layout?" -A default="Use automatic monitor layout" -a "BSP Desktop" -u critical -w "Monitor Layout Is Unset" \
		"There was no monitor layout configuration found!" )
	fi
fi
case "$UserChoice" in
	arandr) which arandr &>/dev/null || notify-send -a "BSP Desktop" -u critical \
			"Arandr Not Found" "You need to install arandr first!" ; exit
		notify-send -a "BSP Desktop" -u critical -w -c "device.error" \
		-h "string:bgcolor:#f6ffce" -h "string:fgcolor:#550000" -h "string:frcolor:#a00000"  \
			"Save with proper file name and path" "After creating your preferred layout, select \"Save as\" from the application menu.
Save it at your home directory as:

$HOME/.screenlayout/monitor.sh"
		arandr
	;;
	default) notify-send -a "BSP Desktop" -u normal -w \
			"Automatic Monitor Layout" "Remember that some desktop elements might not look as it was designed.
It is advised that you use Arandr to set a permanent monitor layout"
;;
esac
