#!/usr/bin/bash

if [ -f $XDG_CONFIG_HOME/nitrogen/bg-saved.cfg ] && which nitrogen &>/dev/null; then
	nitrogen --restore &
elif [ -f $HOME/.fehbg ] && which feh &>/dev/null; then
	sh $HOME/.fehbg &
else
	if [ -z "$DISPLAY" ] ; then
		echo "WARNING: There was no wallpaper configuration found!"
	else
		UserChoice=$(notify-send \
		-A feh="Start Feh to set wallpapers?" \
		-A nitrogen="Start Nitrogen to set wallpapers?" \
		-A later="I will set my wallpapers later" \
		-a "BSP Desktop" \
		-u critical -w \
		"Background Wallpaper Is Unset" \
		"There was no wallpaper configuration found!" )
	fi
fi

# Do what user selected
case "$UserChoice" in
nitrogen) if which nitrogen &>/dev/null ; then
	nitrogen &
	else
		notify-send -a "BSP Desktop" -u critical \
		"Nitrogen Not Found" "You need to install nitrogen first!"
fi
;;
feh) if which feh &>/dev/null ; then
		feh /usr/share/backgrounds/bsp-desktop &
	else
		notify-send -a "BSP Desktop" -u critical \
		"Feh Not Found" "You need to install feh first!"
	fi
;;
esac

