#!/usr/bin/env bash
# To be sourced by bspwm on login
#

# Confirm env xdg_current_desktop
export XDG_CURRENT_DESKTOP=${XDG_CURRENT_DESKTOP:-${DESKTOP_SESSION:-bspwm}}

# merge in defaults and keymaps
userxprofile=$HOME/.xprofile
if [ -f $userxprofile ]; then
    source $userxprofile
else
	# Use fallback xprofile sourcing and env vars
	userresources=$HOME/.Xresources
	usermodmap=$HOME/.Xmodmap
	sysresources=/etc/X11/xinit/.Xresources
	sysmodmap=/etc/X11/xinit/.Xmodmap
	userprofile=$HOME/.profile

	# merge in defaults and keymaps
	if [ -f $sysresources ]; then
		xrdb -merge $sysresources
	fi
	if [ -f $sysmodmap ]; then
		xmodmap $sysmodmap
	fi
	if [ -f "$userresources" ]; then
		xrdb -merge "$userresources"
	fi
	if [ -f "$usermodmap" ]; then
		xmodmap "$usermodmap"
	fi
	if [ -f $userprofile ]; then
		source $userprofile
	fi
	# Make QT apps qt5ct configured
	if [[ "${XDG_SESSION_DESKTOP,,}" =~ "(plasma|kde)" ]] || [[ "${XDG_CURRENT_DESKTOP,,}" == "plasma" ]]; then
		export QT_QPA_PLATFORMTHEME=
		#export QT_AUTO_SCREEN_SCALE_FACTOR=0
		#export PLASMA_USE_QT_SCALING=1
	else
		export QT_QPA_PLATFORMTHEME=qt5ct
	fi

	# Make some apps adjust dpi scale
	export GDK_DPI_SCALE=1.2
	exit 127
fi
