#! /usr/bin/bash

if [ -n "$BSP_INSTALLATION_ERROR" ] ; then
	notify-send -a "BSP Desktop" -u critical -w -c "device.error" \
		-h "string:bgcolor:#f6ffce" -h "string:fgcolor:#550000" -h "string:frcolor:#a00000"  \
			"BSP Desktop Installation Errors" "$BSP_INSTALLATION_ERROR"
fi
