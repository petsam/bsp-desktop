#!/bin/sh

primon=$(xrandr | grep -w connected | grep -w primary | cut -d" " -f1)

# Fallback set top-left monitor as primary
[ -z "${primon}" ] &&
primon=$(xrandr | grep " connected" | grep "[0-9]+0+0" | cut -d" " -f1)

# Print primary monitor name first
[ -n "${primon}" ] && echo $primon
for mon in $(xrandr | grep -w connected | cut -d" " -f1); do
    [ "$mon" != "$primon" ] && echo $mon
done
