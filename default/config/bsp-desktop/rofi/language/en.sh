# Translation key pairs for script usage
# Greek
# LANG=en_US.UTF-8

# Options for powermenu
lock="Lock"
shutdown="Shutdown"
reboot="Reboot"
sleep="Sleep"
logout="Logout"

# Locker prompt and wallpaper
Type_user_pwd="Type the password of user"
Lock_Background="/usr/share/backgrounds/bsp-desktop/corkboard.png"
GreeterColor="054405"
TimeColor="eeeeee"

# Print menu options for screenshot
PrtScrn0="Screen"
PrtScrn1="Area"
PrtScrn2="Window"
WithDelay="with delay"
Seconds="sec"
