# Translation key pairs for script usage
# Greek
# LANG=el_GR.UTF-8

# Options for powermenu
lock="Κλείδωμα"
shutdown="Τερματισμός"
reboot="Επανεκκίνηση"
sleep="Αναστολή"
logout="Έξοδος"

# Screen Locker settings
TypeUserPwd="Πληκτρολογήστε τον κωδικό του χρήστη"
LockBackground="/usr/share/backgrounds/bsp-desktop/corkboard.png"
# Lock_Background="${XDG_PICTURES_DIR}/seadoves.png"
GreeterColor="054405"
TimeColor="eeeeee"

# Print menu options for screenshot
PrtScrn0="Οθόνη"
PrtScrn1="Περιοχή"
PrtScrn2="Παράθυρο"
WithDelay="με καθυστέρηση"
Seconds="δευτ"
