#!/bin/env bash

# Import translations for LANGuage
TrLang=${LANG%%_*}
[ -r $BSP_DESKTOP_HOME/rofi/language/${TrLang:-en}.sh ] &&
	source $BSP_DESKTOP_HOME/rofi/language/${TrLang:-en}.sh 2>/dev/null

rofi -modi window,drun,run -show "${1:-window}" -theme "$BSP_DESKTOP_HOME/rofi/windows.rasi" \
	-hover-select -me-select-entry '' -me-accept-entry MousePrimary \
	-selected-row 1  -window-thumbnail -terminal "$TERMINAL"

