#!/bin/env bash

# Import translations for LANGuage
TrLang=${LANG%%_*}
[ -r $BSP_DESKTOP_HOME/rofi/language/${TrLang:-en}.sh ] &&
	source $BSP_DESKTOP_HOME/rofi/language/${TrLang:-en}.sh 2>/dev/null

# Use tab instead of spaces
# Options for powermenu
lock=" ${lock:-Lock}"
shutdown=" ${shutdown:-Shutdown}"
reboot=" ${reboot:-Reboot}"
sleep=" ${sleep:-Sleep}"
logout=" ${logout:-Logout}"

# Get answer from user via rofi
selected_option=$(echo -e "$lock
$logout
$sleep
$reboot
$shutdown" |
	rofi -dmenu\
             -i \
    -theme $BSP_DESKTOP_HOME/rofi/power-menu.rasi
     )

# Do something based on selected option
if [ "${selected_option:3}" == "${lock:3}" ]
then
	Loggedin=$(userdbctl user $USER 2>/dev/null |
	 sed -n 's/^.*Real Name: \(.*\)/\1/p')
	 Loggedin=${Loggedin:-$USER}
	i3lock -L -k -p default -f -i "$LockBackground" \
	--greeter-text="$TypeUserPwd $Loggedin" \
	--date-color=${GreeterColor:-054405} \
	--{verif,wrong}-color=ffcc77 \
	--time-color=${TimeColor:-eeeeee} \
	--{verif,wrong}-size=32 \
	--keylayout 0 \
	--time-pos="x+700:y+80" \
	--time-size=64 \
	--date-pos="x+30:y+40" \
	--date-size=32 \
	--layout-pos="x+30:y+60" \
	--layout-size=18 \
	--layout-color=${TimeColor:-eeeeee} \
	--{time,date,layout}-align=1 \
	--greeter-align=0 \
	--greeter-color=${GreeterColor:-054405} \
	--greeter-pos=x+800:y+600 \
	--greeter-size=32
# 	\ --no-verify
# no lock, for testing

elif [ "${selected_option:3}" == "${logout:3}" ]
then
    bspc quit
elif [ "${selected_option:3}" == "${shutdown:3}" ]
then
    systemctl poweroff
elif [ "${selected_option:3}" == "${reboot:3}" ]
then
    systemctl reboot
elif [ "${selected_option:3}" == "${sleep:3}" ]
then
#     amixer set Master mute
    systemctl suspend
else
    echo "No match"
fi
